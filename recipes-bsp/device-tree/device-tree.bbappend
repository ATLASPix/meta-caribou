FILESEXTRAPATHS_prepend := "${THISDIR}/patches:"

SRC_URI = "gitsm://gitlab.cern.ch/ATLASPix/peary-firmware.git;protocol=https; \
           file://0001-Add-I2C-muxes-of-the-CaR-board-to-the-device-tree.patch;striplevel=3 \
           "

SRCREV = "${AUTOREV}"

PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/git/outputs/dts/"

KERNEL_DTS_INCLUDE_caribou-zynq7 = "\
			git/outputs/dts \
			"		
COMPATIBLE_MACHINE_caribou-zynq7 = ".*"